const input = document.querySelector('.input')
const form = document.querySelector('.form')

input.addEventListener('blur', () => {
	if (input.value < 0) {
		input.classList.add('error')
		let errorText = document.createElement('p')
		errorText.innerText = 'Please enter correct price'
		document.body.append(errorText)
	} else {
		const price = document.createElement('span')
		const close = document.createElement('button')
		price.innerHTML = `Current price: ${input.value}`
		close.innerText = 'X'
		document.body.prepend(close)
		document.body.prepend(price)
		input.style.color = 'green'
		close.addEventListener('click', () => {
			close.remove()
			price.remove()
		})
	}
})
