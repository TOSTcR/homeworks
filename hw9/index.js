const tabsBtn = document.querySelectorAll('.tabs-title')
const tabsItems = document.querySelectorAll('.tabs__item')

tabsBtn.forEach(element => {
	element.addEventListener('click', () => {
		let currentBtn = element
		let tabId = currentBtn.getAttribute('data-tab')
		let currentTab = document.querySelector(tabId)

		if (!currentBtn.classList.contains('active')) {
			tabsBtn.forEach(element => {
				element.classList.remove('active')
			})

			tabsItems.forEach(element => {
				element.classList.remove('active')
			})

			currentBtn.classList.add('active')
			currentTab.classList.add('active')
		}
	})
})

document.querySelector('.tabs-title').click()
