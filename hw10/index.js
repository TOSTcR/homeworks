const btn = document.querySelectorAll('.fa-eye')
const input = document.querySelectorAll('.input')
const submitButton = document.querySelector('.btn')

btn.forEach(element => {
	element.addEventListener('click', e => {
		if (!element.classList.contains('fa-eye-slash')) {
			element.className = 'fas fa-eye-slash icon-password'
			input.forEach(element => {
				element.setAttribute('type', 'number')
			})
		} else {
			element.className = 'fas fa-eye icon-password'
			input.forEach(element => {
				element.setAttribute('type', 'password')
			})
		}
		e.preventDefault()
	})
})

submitButton.addEventListener('click', () => {
	if (input[0].value === input[1].value) {
		alert('You are welcome')
	} else {
		alert('Нужно ввести одинаковые значения')
	}
})
